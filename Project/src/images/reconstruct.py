# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 20:46:57 2017

@author: prate
"""

import scipy
import scipy.ndimage
import numpy as np 
from PIL import Image
import cv2
import random, os
cnt=0


path =r"C:\Users\prate\Desktop\images"
arr = os.listdir(path)

 # FUNCTION FOR RECONSTRUCTING 2D MATRIX USING PCA
def comp_2d(image_2d):
    global cnt
    global numpc
    cov_mat = image_2d - np.mean(image_2d , axis = 1)
    eig_val, eig_vec = np.linalg.eigh(np.cov(cov_mat)) # USING "eigh", SO THAT PROPRTIES OF HERMITIAN MATRIX CAN BE USED
#    print(eig_val.shape)
#    print(eig_vec.shape)
    p = np.size(eig_vec, axis =1)
    idx = np.argsort(eig_val)
    idx = idx[::-1]
    eig_vec = eig_vec[:,idx]
    eig_val = eig_val[idx]
    while cnt<1:
        print("The principal components are %d"%p)
        numpc = int(input('Enter the principal components less than or equal to principal components'))
        cnt+=1
        
    if numpc <p or numpc >0:
        eig_vec = eig_vec[:, range(numpc)]
        score = np.dot(eig_vec.T, cov_mat)
        recon = np.dot(eig_vec, score) + np.mean(image_2d, axis = 1).T # SOME NORMALIZATION CAN BE USED TO MAKE IMAGE QUALITY BETTER
        recon_img_mat = np.uint8(np.absolute(recon)) # TO CONTROL COMPLEX EIGENVALUES
        
    return recon_img_mat


userinput=int(input('Enter 1-->Img capture 2-->predefined image :'))
if(userinput==1):
    try: 
        os.remove("frame1.jpg")
    except:
        pass
    vidcap = cv2.VideoCapture(0)
    success,image = vidcap.read()
    count = 0
    if(count==0):
        success = True
        count+=1   
    while success:
        success,image = vidcap.read()
        cv2.imwrite("frame%d.jpg" % count, image)
#        imgloc = r"D:\homework and assignments\computer vision\opencv python\frame1.jpg"
        vidcap.release()
        if(count>0):
            success=False 
    b=scipy.ndimage.imread('frame1.jpg')
    c=b.reshape(480,480,4)
    a=c
    
#    img2=cv2.imread('frame1.jpg')
#    imggray=cv2.imread('frame1.jpg',cv2.IMREAD_GRAYSCALE)
#    imgnew=cv2.equalizeHist(imggray)
#    cv2.imwrite('Frame2.jpg',imgnew)
            
if(userinput==2):
    img = random.choice([
    x for x in arr
    if os.path.isfile(os.path.join(path, x))
])
    a = scipy.ndimage.imread(img)
    

# IMPORTING IMAGE USING SCIPY AND TAKING R,G,B COMPONENTS

#a = scipy.ndimage.imread(img)
a_np = np.array(a)
a_r = a_np[:,:,0]
a_g = a_np[:,:,1]
a_b = a_np[:,:,2]





a_r_recon, a_g_recon, a_b_recon = comp_2d(a_r), comp_2d(a_g), comp_2d(a_b) # RECONSTRUCTING R,G,B COMPONENTS SEPARATELY
recon_color_img = np.dstack((a_r_recon, a_g_recon, a_b_recon)) # COMBINING R.G,B COMPONENTS TO PRODUCE COLOR IMAGE
b=np.array(recon_color_img)
recon_color_img = Image.fromarray(recon_color_img)
recon_color_img.show()
new=np.array(recon_color_img)
scipy.misc.imsave("PCA.jpg",new)