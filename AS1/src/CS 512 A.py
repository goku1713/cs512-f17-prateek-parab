import numpy as np
import cmath
def twoA(A_array,B_array,C_array):
    return 2*A_array-B_array

def Q2a(A_array):
     mag=np.linalg.norm(A_array)
     x=A_array[0]
     cosAlpha=x/mag
     alpha=((cmath.acos(cosAlpha)*180)/cmath.pi)
     return alpha
     
def Q3a(A_array):
    X=[]
    mag=np.linalg.norm(A_array)
    for x in A_array:
        newMag=x/mag
        X.append(newMag)
    return X

def Q4a(A_array):
    X=[]
    mag=np.linalg.norm(A_array)
    for x in A_array:
        newMag=x/mag
        X.append(((cmath.acos(newMag))*180)/cmath.pi)
    return X

def Q6a(A_array,B_array):
    mag_a,mag_b=np.linalg.norm(A_array),np.linalg.norm(B_array)
    mag=np.linalg.norm(A_array)
    cosAlpha=np.dot(A_array,B_array)/(mag_a*mag_b)
    Alpha=(cmath.acos(cosAlpha)*180)/cmath.pi
    return Alpha

def Q7a(A_array):
    a,b,c=1,1,-1
    v=[a,b,c]
    v=np.array(v)
    m=np.dot(A_array,v)
    return m

def Q9a(A_array,B_array):
    cross=np.cross(A_array,B_array)
    dot1,dot2=np.dot(cross,A_array),np.dot(cross,B_array)
    if(dot1==dot2):
        pass
    return 0

def Q10a(A_array,B_array,C_array):
    newArr=([A_array,B_array,C_array])
    val=np.linalg.det(newArr)
    if(val<0.000001):
        return 0
    
def Q11a(A_array,B_array):
    A,B=np.transpose(A_array),np.transpose(B_array)
    A=np.dot(A,B_array)
    B=np.dot(B,A_array)
    return (A,B)
    
    
    
    
    
A=[1,2,3]
B=[4,5,6]
C=[-1,1,3]
A_array=np.array(A)
B_array=np.array(B)
C_array=np.array(C)
Q1a=print('\nAnswer of Q1a is',twoA(A_array,B_array,C_array))
Q2a=print('\nAnswer of Q2a is degrees ',Q2a(A_array))
Q3a=print('\nAnswer of Q3a is',Q3a(A_array))
Q4a=print('\nAnswer of Q4a is',Q4a(A_array))
Q5a=print('\nAnswer of Q5a is',np.dot(A_array,B_array))
Q6a=print('\nAnswer of Q6a is',Q6a(A_array,B_array))
Q7a=print('\nAnswer of Q7a is %d thus the vectors are orthogonal'%Q7a(A_array))
Q8a=print('\nAnswer of Q8a is ',np.cross(A_array,B_array))
Q9a=print('\nAnswer of Q9a is %d thus cross pdt is perpendicular to both A and B '%Q9a(A_array,B_array))
Q10a=print('\nAnswer of Q10a is %d thus A,B,C are linearly dependent '%Q10a(A_array,B_array,C_array))
Q11a=print('\nAnswer of Q11a is ',Q11a(A_array,B_array))



