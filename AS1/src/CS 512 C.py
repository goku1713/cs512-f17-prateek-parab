import numpy as np

def Q2c(V,A_array,B_array):
    Vinv=np.linalg.inv(V)
    AV=np.dot(A_array,V)
    final=np.dot(Vinv,AV)
    return final
    

A=[[1,2],[3,2]]
B=[[2,-2],[-2,5]]
v=[[-1,2/3],[1,1]]
u=[[1,2],[-2,1]]
A_array=np.array(A)
B_array=np.array(B)
V=np.array(v)
U=np.array(u)
Q1c=print('\nThe answer of Q1c is\n',np.linalg.eig(A_array))
Q2c=print('\nThe answer of Q2c is\n',Q2c(V,A_array,B_array))
Q3c=print('\nThe answer of Q3c is\n',np.dot(V[:,0],V[:,1]))
Q4c=print('\nThe answer of Q4c is\n',np.linalg.eig(B_array))
Q5c=print('\nThe answer of Q5c is %d therefore the 2 vectors are orthogonal since their dot product is zero\n'%np.dot(U[:,0],U[:,1]))