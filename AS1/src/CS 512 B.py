import numpy as np

def Q1b(A_array,B_array):
    return 2*A_array-B_array

def Q2b(A_array,B_array):
    AB=np.dot(A_array,B_array)
    BA=np.dot(B_array,A_array)
    return(AB,BA)
    
def Q3b(A_array,B_array):
    AB=np.dot(A_array,B_array)
    ABT=np.transpose(AB)
    AT=np.transpose(A)
    BT=np.transpose(B)
    ATBT=np.dot(BT,AT)
    if(ATBT == ABT).all():
        return ABT

def Q4b(A_array,C_array):
    detA,detC=round(np.linalg.det(A_array)),round(np.linalg.det(C_array))
    return (detA,detC)

def Q5b(A_array,B_array,C_array):
    AT=np.transpose(A_array)
    BT=np.transpose(B_array)
    ATB=np.dot(AT,B_array)
    ATC=np.dot(AT,C_array)
    BTC=np.dot(BT,C_array)
    if(ATB==ATC).all() and(ATC==BTC).all():
        return ('orthogonal')
    else:
        return(' not Orthogonal')

def Q6b(A_array,B_array):
    Ainv=np.linalg.inv(A_array)
    Binv=np.linalg.inv(B_array)
    return (Ainv,Binv)






A=[[1,2,3],[4,-2,3],[0,5,-1]]
B=[[1,2,1],[2,1,-4],[3,-2,1]]
C=[[1,2,3],[4,5,6],[-1,1,3]]
A_array=np.array(A)
B_array=np.array(B)
C_array=np.array(C)
Q1b=print('\nThe answer of Q1b is\n',Q1b(A_array,B_array))
Q2b=print('\nThe answer of Q2b is\n',Q2b(A_array,B_array))
Q3b=print('\nThe answer of Q3b is\n',Q3b(A_array,B_array))
Q4b=print('\nThe answer of Q4b is\n',Q4b(A_array,C_array))
Q5b=print('\nThe answer of Q5b is\n',Q5b(A_array,C_array,C_array))
Q6b=print('\nThe answer of Q6b is\n',Q6b(A_array,B_array))



